<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BlogPostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_post', function (Blueprint $table) {
            $table->increments('id');
            $table->string('theme');
            $table->text('content');
            $table->text('demo_content');
            $table->text('meta_keywords');
            $table->text('meta_description');
            $table->string('url')->unique();
            $table->integer('author_id');
            $table->integer('views');
            $table->tinyInteger('show');
            $table->timestamps();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('blog_post');
    }
}
