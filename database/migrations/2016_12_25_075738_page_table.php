<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('page', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('meta_keywords');
            $table->text('meta_description');
            $table->string('url')->unique();
            $table->string('redirect');
            $table->text('description');
            $table->text('settings');
            $table->integer('order');
            $table->integer('views');
            $table->integer('show');
            $table->integer('level');
            $table->integer('parent');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('page');
    }
}
