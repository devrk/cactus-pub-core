@extends('layouts.admin')

@section('title')
    {{trans('admin.page_list')}}
@endsection

@section('header_scripts')
@endsection

@section('main_content')
    <div class="row">
        <div class="col-lg-12 ">
            <h3 class="page-header">
                <div class="row">
                    <div class="col-lg-10">{{trans('admin.page_list')}}</div>
                    <div class="col-lg-2">
                        <a href="{{url('/admin/add_page')}}"
                           class="btn btn-outline btn-primary">{{trans('admin.add_page')}}</a>
                    </div>
                </div>
            </h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="table-responsive">
        @if (isset($pages) && ! empty($pages->all()))
            <table id="categories" class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>{{trans('admin.page_name')}}</th>
                    <th>{{trans('admin.page_status')}}</th>
                    <th>{{trans('admin.page_updated')}}</th>
                    <th>{{trans('admin.page_created')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($pages->all() as $page)
                    <tr>
                        <td>{{$page['id']}}</td>
                        <td><a href="{{url('/admin/page/' . $page['id'] )}}">{{$page['name']}}</a></td>
                        <td>
                            @if ($page['show']==1)
                                <span class="text-success">{{trans('admin.on')}}</span>
                            @else
                                <span class="text-danger">{{trans('admin.off')}}</span>
                            @endif
                        </td>
                        <td>{{strftime("%H:%M %d-%m-%Y", strtotime($page['updated_at']))}}</td>
                        <td>{{strftime("%H:%M %d-%m-%Y", strtotime($page['created_at']))}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $pages->links() }}
        @else
            {{trans('admin.no_pages')}}
        @endif
    </div>
@endsection