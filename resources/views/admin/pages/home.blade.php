@extends('layouts.admin')

@section('title')
    {{trans('admin.home')}}
@endsection

@section('header_scripts')

@endsection

@section('main_content')
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">{{trans('page.home_page')}}</h1>
    </div>
</div>
<form role="form" method="post" enctype="multipart/form-data">
    {{ csrf_field() }}
    {{ method_field('POST') }}
    {{--<div class="row form-group">
        <div class="col-xs-12 col-sm-12 col-md-2">{{trans('admin.post_category')}}</div>
        <div class="col-xs-12 col-sm-12 col-md-10">
            <select class="selectpicker" name="category" data-live-search="true" title="{{trans('admin.enter_post_category')}}">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
            </select>
        </div>
    </div>--}}
    <div class="row form-group">
        <div class="col-xs-12 col-sm-12 col-md-2">{{trans('page.title')}}</div>
        <div class="col-xs-12 col-sm-12 col-md-10">
            <input class="form-control" type="text" value="@if(isset($page) && !empty($page)){{$page['name']}}@endif" name="name" placeholder="{{trans('page.enter_title')}}">
        </div>
    </div>
    <div class="row form-group">
        <div class="col-xs-12 col-sm-12 col-md-2">{{trans('page.meta_words')}}</div>
        <div class="col-xs-12 col-sm-12 col-md-10">
            <input class="form-control" type="text" value="@if(isset($page) && !empty($page)){{$page['meta_keywords']}}@endif" name="meta_keywords" placeholder="{{trans('page.enter_meta_words')}}">
        </div>
    </div>
    <div class="row form-group">
        <div class="col-xs-12 col-sm-12 col-md-2">{{trans('page.meta')}}</div>
        <div class="col-xs-12 col-sm-12 col-md-10">
            <textarea class="form-control" name="meta_description" placeholder="{{trans('page.short_enter_description')}}">@if(isset($page) && !empty($page)){{$page['meta_description']}}@endif</textarea>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-xs-12 col-sm-12 col-md-2">{{trans('page.redirect')}}</div>
        <div class="col-xs-12 col-sm-12 col-md-10">
            <input class="form-control" type="text" value="@if(isset($page) && !empty($page)){{$page['redirect']}}@endif" name="redirect" placeholder="{{trans('page.enter_redirect')}}">
        </div>
    </div>
    <button type="submit" class="btn btn-outline btn-success">{{trans('admin.save')}}</button>
    <button type="reset" class="btn btn-outline btn-default">{{trans('admin.reset')}}</button>
</form>
@endsection
@section('footer_scripts')
    <script src="/admin/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: 'textarea',
            language_url: '/admin/tinymce/lang/uk.js',
            height: 200,
            fontsize_formats: '8px 10px 12px 14px 18px 24px 36px',
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'insertfile undo redo | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',

        });
    </script>
@endsection