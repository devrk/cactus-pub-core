@extends('layouts.admin')

@section('title')
    {{trans('admin.system_page')}}
@endsection

@section('header_scripts')
@endsection

@section('main_content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">{{trans('page.home_page')}}</h1>
        </div>
    </div>

    <form role="form" method="post" action="/admin/save_page/{{$page['id']}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        {{ method_field('POST') }}

        <div class="row form-group">
            <div class="col-xs-12 col-sm-12 col-md-2">{{trans('page.title')}}</div>
            <div class="col-xs-12 col-sm-12 col-md-10">
                <input class="form-control" type="text" value="{{$page['name']}}" name="name" placeholder="{{trans('page.enter_title')}}">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xs-12 col-sm-12 col-md-2">{{trans('page.url')}}</div>
            <div class="col-xs-12 col-sm-12 col-md-10">
                <input class="form-control" type="text" value="{{$page['url']}}" name="url" placeholder="{{trans('page.enter_url')}}">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xs-12 col-sm-12 col-md-2">{{trans('page.redirect')}}</div>
            <div class="col-xs-12 col-sm-12 col-md-10">
                <input class="form-control" type="text" value="{{$page['redirect']}}" name="redirect" placeholder="{{trans('page.enter_redirect')}}">
            </div>
        </div>

        <div class="row form-group">
            <div class="col-xs-12 col-sm-12 col-md-2">{{trans('page.parent')}}</div>
            <div class="col-xs-12 col-sm-12 col-md-10">
                <select class="selectpicker" name="parent" data-live-search="true" title="{{trans('page.enter_parent')}}">
                    <option value="0">{{trans('page.no_parent')}}</option>
                    @if(! empty($parents))
                        @foreach($parents as $parent)
                            <option value="{{$parent['id']}}" @if($parent['id'] == $page['parent']) selected="selected" @endif>{{$parent['name']}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>

        <div class="row form-group">
            <div class="col-xs-12 col-sm-12 col-md-2">{{trans('page.order')}}</div>
            <div class="col-xs-12 col-sm-12 col-md-10">
                <input class="form-control" type="text" value="{{$page['order']}}" name="order" placeholder="{{trans('page.enter_order')}}">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xs-12 col-sm-12 col-md-2">{{trans('page.level')}}</div>
            <div class="col-xs-12 col-sm-12 col-md-10">
                <input class="form-control" type="text" value="{{$page['level']}}" name="level" placeholder="{{trans('page.enter_redirect')}}">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xs-12 col-sm-12 col-md-2">{{trans('page.meta_words')}}</div>
            <div class="col-xs-12 col-sm-12 col-md-10">
                <input class="form-control" type="text" value="{{$page['meta_keywords']}}" name="meta_keywords" placeholder="{{trans('page.enter_meta_words')}}">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xs-12 col-sm-12 col-md-2">{{trans('page.meta')}}</div>
            <div class="col-xs-12 col-sm-12 col-md-10">
                <textarea class="form-control" name="meta_description" placeholder="{{trans('page.meta_enter_description')}}">{{$page['meta_description']}}</textarea>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xs-12 col-sm-12 col-md-2">{{trans('page.description')}}</div>
            <div class="col-xs-12 col-sm-12 col-md-10">
                <textarea class="form-control" name="description" placeholder="{{trans('page.enter_description')}}">{{$page['description']}}</textarea>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xs-12 col-sm-12 col-md-2">{{trans('page.show')}}</div>
            <div class="col-xs-12 col-sm-12 col-md-10">
                <label>
                    <input value="1" name="show" @if($page['show'] == 1) checked @endif type="checkbox">
                </label>
            </div>
        </div>
        <button type="submit" class="btn btn-outline btn-success">{{trans('admin.save')}}</button>
        <button type="reset" class="btn btn-outline btn-default">{{trans('admin.reset')}}</button>
    </form>
@endsection
@section('footer_scripts')
{{--    <script src="/admin/tinymce/tinymce.min.js"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: 'textarea',
            language_url: '/admin/tinymce/lang/uk.js',
            height: 200,
            fontsize_formats: '8px 10px 12px 14px 18px 24px 36px',
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'insertfile undo redo | styleselect | fontsizeselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',

        });
    </script>--}}
@endsection