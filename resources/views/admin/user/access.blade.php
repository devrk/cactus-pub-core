@extends('layouts.admin')

@section('title')
    {{trans('admin.access_list')}}
@endsection

@section('footer_scripts')
@endsection

@section('main_content')
    <div class="row">
        <div class="col-lg-12 ">
            <h3 class="page-header">
                <div class="row">
                    <div class="col-lg-10">{{trans('admin.access_list')}}</div>
                    <div class="col-lg-2">
                        <a href="{{url('/control/user/add_role')}}"
                           class="btn btn-outline btn-primary">{{trans('admin.add_access')}}</a>
                    </div>
                </div>
            </h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="table-responsive">
        @if (isset($role) && ! empty($role))
            <table id="categories" class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>{{trans('admin.post_name')}}</th>
                    <th>{{trans('admin.post_updated')}}</th>
                    <th>{{trans('admin.post_created')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($role as $item)
                    <tr>
                        <td>{{$item['r_id']}}</td>
                        <td><a href="{{url('/control/user/role/' . $item['r_id'] )}}">{{$item['name']}}</td>
                        <td>{{strftime("%H:%M %d-%m-%Y", strtotime($item['updated_at']))}}</td>
                        <td>{{strftime("%H:%M %d-%m-%Y", strtotime($item['created_at']))}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $role->links() }}
        @else
            {{trans('admin.no_roles')}}
        @endif
    </div>
@endsection