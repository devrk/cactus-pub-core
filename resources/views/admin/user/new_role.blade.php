@extends('layouts.admin')

@section('title')
    {{trans('admin.add_role_heading')}}
@endsection
@section('header_scripts')
@endsection

@section('footer_scripts')
@endsection
@section('main_content')
    <div class="row">
        <div class="col-lg-12 ">
            <h3 class="page-header">{{trans('admin.add_role_heading')}}</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form role="form" method="post" enctype="multipart/form-data" >
        {{ csrf_field() }}
        <div class="row form-group">
            <div class="col-xs-12 col-sm-12 col-md-2">{{trans('admin.role_name')}}</div>
            <div class="col-xs-12 col-sm-12 col-md-10">
                <input class="form-control" type="text" value="" name="name" placeholder="{{trans('admin.enter_role_name')}}">
            </div>
        </div>
        @foreach($available_routes as $controller => $data)
            @foreach($data as $value)        <div class="row form-group">
            <div class="col-xs-4 col-sm-4 col-md-1">
                <div class="checkbox">
                    <label>
                        <input value="{{$value['path']}}" type="checkbox" name="route[{{$controller . '@' . $value['action'] }}]" >
                        {{$controller . '@' . $value['action']}}
                    </label>
                </div>
            </div>
        </div>
        @endforeach
        @endforeach


        <button type="submit" class="btn btn-outline btn-success">{{trans('admin.save')}}</button>
        <button type="reset" class="btn btn-outline btn-default">{{trans('admin.reset')}}</button>
    </form>
@endsection