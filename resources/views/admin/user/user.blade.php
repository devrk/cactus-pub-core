@extends('layouts.admin')

@section('title')
    {{trans('admin.upd_user_heading')}}
@endsection

@section('footer_scripts')
@endsection

@section('main_content')
    <div class="row">
        <div class="col-lg-12 ">
            <h3 class="page-header">{{trans('admin.upd_user_heading')}}</h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form role="form" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="row form-group">
            <div class="col-xs-12 col-sm-12 col-md-2">{{trans('admin.user_name')}}</div>
            <div class="col-xs-12 col-sm-12 col-md-10">
                <input class="form-control" type="text" value="{{$user['name']}}" name="name" placeholder="{{trans('admin.enter_user_name')}}">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xs-12 col-sm-12 col-md-2">{{trans('admin.user_email')}}</div>
            <div class="col-xs-12 col-sm-12 col-md-10">
                <input class="form-control" type="email" value="{{$user['email']}}" name="email" placeholder="{{trans('admin.enter_user_email')}}">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xs-12 col-sm-12 col-md-2">{{trans('admin.user_password')}}</div>
            <div class="col-xs-12 col-sm-12 col-md-10">
                <input class="form-control" type="password" value="" name="password" placeholder="{{trans('admin.enter_user_password')}}">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xs-12 col-sm-12 col-md-2">{{trans('admin.user_role')}}</div>
            <div class="col-xs-12 col-sm-12 col-md-10">
                <select class="selectpicker" name="role" data-live-search="true" title="{{trans('admin.enter_user_role')}}">
                    @foreach($roles as $role)
                        <option value="{{$role['r_id']}}" @if($user['role'] == $role['r_id']) selected="selected" @endif>{{$role['name']}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row form-group">
            <div class="col-xs-7 col-sm-4 col-md-2">{{trans('admin.user_enable')}}</div>
            <div class="col-xs-5 col-sm-8 col-md-10">
                <div class="checkbox">
                    <label>
                        <input value="1" type="checkbox" name="active" @if($user['active'] == 1) checked="checked" @endif>
                    </label>
                </div>
            </div>
        </div>

        <button type="submit" class="btn btn-outline btn-success">{{trans('admin.save')}}</button>
        <button type="reset" class="btn btn-outline btn-default">{{trans('admin.reset')}}</button>
        <a href="{{url('/control/user/delete/' . $user['id'])}}" id="delete" type="button" class="btn btn-outline btn-danger">{{trans('admin.delete')}}</a>
    </form>
@endsection