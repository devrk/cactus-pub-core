@extends('layouts.admin')

@section('title')
    {{trans('admin.user_list')}}
@endsection

@section('footer_scripts')
@endsection

@section('main_content')
    <div class="row">
        <div class="col-lg-12 ">
            <h3 class="page-header">
                <div class="row">
                    <div class="col-lg-10">{{trans('admin.user_list')}}</div>
                    <div class="col-lg-2">
                        <a href="{{url('/control/user/add')}}"
                           class="btn btn-outline btn-primary">{{trans('admin.add_user')}}</a>
                    </div>
                </div>
            </h3>
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div class="table-responsive">
        @if (isset($users) && ! empty($users))
            <table id="users" class="table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>{{trans('admin.user_name')}}</th>
                    <th>{{trans('admin.user_email')}}</th>
                    <th>{{trans('admin.access_status')}}</th>
                    <th>{{trans('admin.updated')}}</th>
                    <th>{{trans('admin.created')}}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $item)
                    <tr>
                        <td>{{$item['id']}}</td>
                        <td><a href="{{url('/control/user/upd/' . $item['id'] )}}">{{$item['name']}}</a></td>
                        <td>{{$item['email']}}</td>
                        <td>@if($item['active'] == 1) {{trans('admin.on')}} @else {{trans('admin.off')}} @endif</td>
                        <td> {{strftime("%H:%M %d-%m-%Y", strtotime($item['updated_at']))}}</td>
                        <td>{{strftime("%H:%M %d-%m-%Y", strtotime($item['created_at']))}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $users->links() }}
        @else
            {{trans('admin.no_users')}}
        @endif
    </div>
@endsection