    <!doctype html>
<html lang="ua">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <meta name="keywords" content="@yield('meta_keywords')"/>
    <meta name="description" content="@yield('meta_description')"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="/css/stylesheet.css" rel="stylesheet">
    <link rel="shortcut icon" href="/favicon.ico" />

    {{--<script type="text/javascript" src="/pub/js/plugins/modernizr.custom.79639.js"></script>--}}
    {{--<script type="text/javascript" src="/pub/js/jquery2.2.3.js"></script>--}}
    @yield('google_analytics')
    @yield('yandex_metrika')

    @yield('header_includes')
</head>

@if (Auth::check())
    <div class="fixed-nav">
        <div><a href="{{ url('/admin/panel') }}">{{trans('front.dashboard')}}</a></div>
        <div><a href="{{ url('/logout') }}">{{trans('front.exit')}}</a></div>
    </div>
@endif
<div class="content">
@yield('main_content')
@yield('bottom_content')
    <header>
        <div class="top-header">
            <a class="top-left-home" href="#">
                <div class="left-arrow"></div>
                <div class="home-icon"></div>
            </a>
            <div class="top-right-buttons">
                <div class="header-button text"><a href="#">blog</a></div>
                <div class="header-button facebook"></div>
                <div class="header-button vkontakte"></div>
            </div>
        </div>
        <div class="horizontal divider"></div>
        <div class="bottom-header">
            <div class="logo">
                <a href="#">
                    <span class="white">комп'ютерний сервіс</span>
                    <span class="green">кактус</span>
                </a>
            </div>
            <div class="contacts green">
                <ul>
                    <li class="kyivstar">(068) 315 10 10</li>
                    <li class="lifecell">(068) 315 10 10</li>
                    <li class="vodafone">(068) 315 10 10</li>
                </ul>
            </div>
        </div>
    </header>
    <div class="nav-wrapper">
        <nav class="main-menu">
            <div class="item"><a href="#">Ремонт Apple</a></div>
            <div class="item"><a href="#">Ремонт ПК</a></div>
            <div class="item"><a href="#">Ремонт ноутбуків</a></div>
            <div class="item"><a href="#">Ремонт планшетів</a></div>
            <div class="item"><a href="#">Ремонт телефонів</a></div>
            <div class="item"><a href="#">Додаткові послуги</a></div>
            <div class="item"><a href="#">Контакти</a></div>
        </nav>
    </div>
    <div class="page-wrap">
        <div class="main-screen">
            <div class="content"></div>
            <div class="order" style="display: none;">
                <div class="text-main-order">Оформіть заявку<br/>на <span>безеоштовну</span>консультацію<br/>і отримайте <span>знижку</span> 10% на ремонт</div>
                <div class="button-main-order icon right-flag">Замовити</div>
            </div>
        </div>
        <div class="items-wrap">
            <div class="header">
                <h1>Як ми працюємо:</h1>
            </div>
            <div class="list-wrap">
                <ul>
                    <li class="data">
                        <div class="icons">
                            <div class="icon lead"></div>
                        </div>
                        <div class="text">Заявка клієнта</div>
                    </li>
                    <li class="data small">
                        <div class="icons">
                            <div class="icon right-arrow"></div>
                        </div>
                        <div class="text"></div>
                    </li>
                    <li class="data">
                        <div class="icons">
                            <div class="icon call"></div>
                        </div>
                        <div class="text">Дзвінок оператора. Консультація.</div>
                    </li>
                    <li class="data small">
                        <div class="icons">
                            <div class="icon right-arrow"></div>
                        </div>
                        <div class="text"></div>
                    </li>
                    <li class="data">
                        <div class="icons">
                            <div class="icon delivery"></div>
                        </div>
                        <div class="text">Доставка техніки в сервісний центр</div>
                    </li>
                    <li class="data small">
                        <div class="icons">
                            <div class="icon right-arrow"></div>
                        </div>
                        <div class="text"></div>
                    </li>
                    <li class="data">
                        <div class="icons">
                            <div class="icon diagnostic"></div>
                        </div>
                        <div class="text">Діагностика. Дзвінок менеджера (Узгодження)</div>
                    </li>
                    <li class="data small">
                        <div class="icons">
                            <div class="icon right-arrow"></div>
                        </div>
                        <div class="text"></div>
                    </li>
                    <li class="data">
                        <div class="icons">
                            <div class="icon repair"></div>
                        </div>
                        <div class="text">Виконання ремонту</div>
                    </li>
                    <li class="data small">
                        <div class="icons">
                            <div class="icon right-arrow"></div>
                        </div>
                        <div class="text"></div>
                    </li>
                    <li class="data">
                        <div class="icons">
                            <div class="icon pay"></div>
                        </div>
                        <div class="text">Оплата. Повернення вже справної техніки</div>
                    </li>
                </ul>
            </div>

        </div>

        <div class="request-wrap">
            <div class="header">
                <h1>У вас залишися запитання? Задайте їх нашому спеціалісту:</h1>
            </div>
            <div class="form-wrap">
                <form action="#">
                    <input type="text" name="email" placeholder="Ваш номер телефону"/><button type="button">Зробити запит</button>
                </form>
            </div>

        </div>
        <div class="feedbacks-wrap">
            <div class="header">
                <h1>Відгуки наших клієнтів</h1>
            </div>
            <div class="feedbacks-grid">
                <div class="feedback">
                    <div class="shadow">
                        <div class="marker"></div>
                        <div class="message">
                            <div class="message-text">feedback feedback feedback feedback feedback feedback feedback
                                feedback feedback feedback feedback feedback feedback feedback feedback
                            </div>
                            <div class="info-wrap">
                                <div class="message-date">11 липня 2016</div>
                                <div class="message-author">Ян Тарновський</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="feedback">
                    <div class="shadow">
                        <div class="marker"></div>
                        <div class="message">
                            <div class="message-text">feedback feedback feedback feedback feedback feedback feedback
                                feedback feedback feedback feedback feedback feedback feedback feedback
                            </div>
                            <div class="info-wrap">
                                <div class="message-date">11 липня 2016</div>
                                <div class="message-author">Ян Тарновський</div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="feedback">
                    <div class="shadow">
                        <div class="marker"></div>
                        <div class="message">
                            <div class="message-text">feedback feedback feedback feedback feedback feedback feedback
                                feedback feedback feedback feedback feedback feedback feedback feedback
                            </div>
                            <div class="info-wrap">
                                <div class="message-date">11 липня 2016</div>
                                <div class="message-author">Ян Тарновський</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="message-button-wrap">
                <div class="message-button-holder">
                    <a href="#"><i class="green chat icon"></i><span>text</span></a>
                    <a href="#"><i class="green mail icon"></i><span>text</span></a>
                </div>
            </div>
        </div>
        <div class="location">
            <div class="location-message"><h3>Як до нас дістатись?</h3></div>
            <div id="location-map" class="location-map"></div>
        </div>
    </div>


    <footer>
        <div class="footer-info-top">
            <div class="footer-info-wrap">
                <div class="info-block">
                    <div class="title">
                        <h3>Адреса</h3>
                    </div>
                    <div class="about"><i class="gray-pointer icon"></i><span>Україна, Тернопіль вул.Крушельницької 18, офіс 901</span>
                    </div>
                </div>
                <div class="info-block">
                    <div class="title">
                        <h3>Телефони</h3>
                    </div>
                    <div class="about">
                        <i class="gray-phone icon"></i>
                        <span>+38 (068) 315 10 10<br/>
                            +38 (068) 315 10 10<br/>
                            +38 (068) 315 10 10
                        </span>
                    </div>
                </div>
                <div class="info-block">
                    <div class="title">
                        <h3>Пошта</h3>
                    </div>
                    <div class="about">
                        <i class="gray-mail icon"></i>
                        <span>office@cactus.ua</span>
                    </div>
                </div>
                <div class="info-block">
                    <div class="title">
                        <h3>Графік роботи</h3>
                    </div>
                    <div class="about">
                        <i class="gray-clock icon"></i>
                        <span class="wide">Понеділок - П'ятниця 9:00-19:00<br/>
                            Субота - 9:00-17:00<br/>
                            Неділя - вихідний</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-info-bottom">Сервіс, який приємно дивує</div>
    </footer>
</div>

<script type="text/javascript" src="/js/app.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBq644xFOidJq5vr2hsExSdN0NPhiv91zo&callback=initMap" async defer></script>
@yield('footer_includes')
</body>
</html>
