<!DOCTYPE html>
<html lang="ua">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>@yield('title')</title>

    <link rel="shortcut icon" href="/pub/favicon_sw.ico" />
    <!-- Bootstrap Core CSS -->
    <link href="/admin/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="/admin/css/metisMenu.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="/admin/css/timeline.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="/admin/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="/admin/css/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="/admin/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <link href="/admin/css/bootstrap-select.min.css" rel="stylesheet" type="text/css">

    <link href="/admin/css/stylesheet.css" rel="stylesheet" type="text/css">


    <link href="/admin/jqGrid/css/ui.jqgrid.css" rel="stylesheet" type="text/css">
    <link href="/admin/jqGrid/css/ui.jqgrid-bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/admin/jqGrid/css/ui.jqgrid-bootstrap-ui.css" rel="stylesheet" type="text/css">
    <link href="/admin/css/stylesheet.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <link href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
    <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
    <![endif]-->
    @yield('header_scripts')

</head>

<body>

<div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">

            <a class="navbar-brand" href="{{ url('/') }}">{{trans('admin.layout_title')}}</a>
        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <form action="/logout" method="post">
                            {{csrf_field()}}
                            <button type="submit"><i class="fa fa-sign-out fa-fw"></i> {{trans('admin.exit')}}</button>
                        </form>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li>
                        <a href="{{ url('/admin/panel') }}" @if($controller=='PanelController' && $action=='index') class="active" @endif><i class="fa fa-dashboard fa-fw"></i> {{trans('admin.dashboard')}}</a>
                    </li>
                    <li @if($controller == 'SystemPageController') class="active" @endif>
                        <a href="#">
                            <i class="fa fa-sitemap fa-fw"></i> {{trans('admin.system_pages')}}<span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-second-level collapse @if($controller == 'SystemPageController') in @endif" aria-expanded="true" style="">
                            <li>
                                <a @if($controller=='SystemPageController' && $action == 'mainPage') class="active" @endif href="{{ url('/admin/main_page') }}">{{trans('admin.main_page')}}</a>
                            </li>
                            <li>
                                <a @if($controller=='SystemPageController' && in_array($action, ['pageList', 'page', 'addPage'])) class="active" @endif href="{{ url('/admin/system_pages') }}">{{trans('admin.system_pages_list')}}</a>
                                <!-- /.nav-third-level -->
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>

    <div id="page-wrapper">
        @yield('main_content')
        <div class="bottom-plain-block"></div>
    </div>

    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="/admin/js/jquery/jquery2.2.3.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/admin/js/bootstrap/bootstrap.min.js"></script>
<script src="/admin/js/select/bootstrap-select.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="/admin/js/metisMenu.js"></script>

<!-- Morris Charts JavaScript -->
<script src="/admin/js/raphael.js"></script>

<!-- Custom Theme JavaScript -->
<script src="/admin/js/sb-admin-2.js"></script>

@yield('footer_scripts')

</body>

</html>
