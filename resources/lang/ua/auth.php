<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'      => 'Ці дані не співпадають із жодними існуючими.',
    'throttle'    => 'Забагато спроб входу. удь ласка спробуйте через :seconds секунд.',
    'home'        => 'Домашня сторінка',
    'login'       => 'Вхід',
    'register'    => 'Реєстрація',
    'remember'    => 'Запа\'ятати мене',
    'forgot_pass' => 'Забули пароль?',
    'email'       => 'E-Mail адреса',
    'password'    => 'Пароль'
];
