<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Frontend Language Lines
    |--------------------------------------------------------------------------
    |
    |---------------------------------------------------------------------------
    | Menu items
    |---------------------------------------------------------------------------
    */

    'home'             => 'Головна',
    'catalog'          => 'Каталог робіт',
    'become_an_author' => 'Стати автором',
    'work_order'       => 'Замовити роботу',
    'contact'          => 'Контакти',
    'search'           => 'Пошук',
    'pricing'          => 'Ціни',
    'payment'          => 'Оплата',

    /*
    |---------------------------------------------------------------------------
    | Title items
    |---------------------------------------------------------------------------
    */

    'home_title'             => 'Student Work: Головна',
    'catalog_title'          => 'Student Work: Каталог робіт',
    'become_an_author_title' => 'Student Work: Стати автором',
    'work_order_title'       => 'Student Work: Замовити роботу',
    'contact_title'          => 'Student Work: Контакти',
    'search_title'           => 'Student Work: Пошук',
    'pricing_title'          => 'Student Work: Ціни',
    'payment_title'          => 'Student Work: Оплата',

    /*
    |---------------------------------------------------------------------------
    | Fixed menu
    |---------------------------------------------------------------------------
    */

    'dashboard' => 'Панель адміністрування',
    'exit'      => 'Вихід',
    /*
    |---------------------------------------------------------------------------
    | Bottom menu
    |---------------------------------------------------------------------------
    */

    'about_us' => 'Про нас',
    'sitemap' => 'Карта сайту',

    /*
    |---------------------------------------------------------------------------
    | Buttons
    |---------------------------------------------------------------------------
    */

    'make_order'     => 'Замовити роботу',
    'view_feedbacks' => 'Переглянути відгуки',
    'view_feedback'  => 'Показати відгук',
    'add_feedback'   => 'Додати відгук',
    'view_catalog'   => 'Переглянути виконані роботи',
    /*
    |---------------------------------------------------------------------------
    | Action buttons
    |---------------------------------------------------------------------------
    */

    'add'              => 'Додати',
    'accept'           => 'Прийняти',
    'confirm'          => 'Підтвердити',
    'delete'           => 'Видалити',
    'create'           => 'Створити',
    'save'             => 'Зберегти',
    'back'             => 'Назад',
    'cancel'           => 'Скасувати',
    'reset'            => 'Скинути',
    'on'               => 'Вкл',
    'off'              => 'Викл',
    'undefined'        => 'Невідомо',
    'unset'            => 'Не вказано',

    /*
    |---------------------------------------------------------------------------
    | Category translate
    |---------------------------------------------------------------------------
    */
    'categories'                 => 'Категорії',
    'categories_list'            => 'Перелік категорій',
    'no_categories'              => 'Немає категорій',

    'category_description'       => 'Опис категорії',

    /*
    |---------------------------------------------------------------------------
    | Post translate
    |---------------------------------------------------------------------------
    */
    'posts_list'             => 'Перелік публікацій',
    'no_posts'               => 'Немає публікацій',

    'post_updated'           => 'Дата оновлення',
    'post_created'           => 'Дата створення',

    /*
    |---------------------------------------------------------------------------
    | Index modal
    |---------------------------------------------------------------------------
    */

    'leave_a_feedback' => 'Оцініть нашу роботу',
    'feedback_name'    => 'Ваше імя',
    'feedback_mail'    => 'Ваш e-mail',
    'feedback_rating'  => 'Оцінка роботи',
    'feedback_message' => 'Ваш коментар',
    'feedback_thanks'  => 'Дякуємо за ваш відгук!',

    'feedback_star_very_bad' => 'Дуже погано',
    'feedback_star_bad'      => 'Погано',
    'feedback_star_pure'     => 'Задовільно',
    'feedback_star_good'     => 'Добре',
    'feedback_star_great'    => 'Чудово',
    /*
    |---------------------------------------------------------------------------
    | Search
    |---------------------------------------------------------------------------
    */
    'enter_search'   => 'Введіть параметр пошуку',
    'found'          => 'Знайдено :count резульатів за запитом <strong>":param"</strong>',
    'go_search'      => 'Шукати',

    /*
    |---------------------------------------------------------------------------
    | For authors
    |---------------------------------------------------------------------------
    */
    'authors_heading_main'   => 'Виконання робiт на замовлення',
    'authors_hello_text'     => 'У вас є досвід написання робіт і вільний час? Ми зможемо з Вами співпрацювати. Ми ставимо високі вимоги перед кандидатами, оскільки формуємо команду професійних авторів, що зможуть отримувати гідну платню за свою інтелектуальну працю. Тому, якщо Ви виконуєте роботи якісно і за помірними цінами - у Вас будуть постійні замовлення і стабільний прибуток.',
    'authors_offers_heading' => 'Наші гарантії',
    'authors_offer_1'        => 'Виплата без затримок і комісій',
    'authors_offer_2'        => 'Персональний менеджер',
    'authors_offer_3'        => 'Вартість написання встановлюєте Ви',
    'authors_offer_4'        => 'Вільний графік роботи',
    'authors_offer_5'        => 'Постійна наявність замовлень',
    'authors_offer_6'        => 'Зручний інтерфейс для авторів',
    'authors_show_form'      => 'Почати співпрацю',
    'authors_show_all'       => 'Повні умови співпраці',
    'authors_go_form'        => 'Оформити заявку',
    'authors_phone'          => 'Телефон',
    'authors_mail'           => 'E-mail',
    'authors_skype'          => 'Skype',
    'authors_specialization' => 'Основна спеціалізація (відповідає отриманій освіті)',
    'authors_works'          => 'Види виконаних робіт',
    'authors_experience'     => 'Досвід написання студентських робіт',
    'authors_spec_list'      => 'Перелік можливих дисциплін',
    'authors_spec_ask'       => 'Бажані типи робіт',
    'authors_message'        => 'Ваше повідомлення',
    'authors_thanks'         => 'Дякуємо за вашу заявку. Скоро із вами звяжеться наш менеджер.',
    /*
    |---------------------------------------------------------------------------
    | Orders
    |---------------------------------------------------------------------------
    */

    'order_make'         => 'Зробіть замовлення роботи',
    'order_make_explain' => 'Після замовлення з вами зв\'яжеться менеджер у найкоротші терміни',
    'order_theme'        => 'Тема',
    'order_type'         => 'Тип роботи',
    'order_city'         => 'Місто',
    'order_school'       => 'Навчальний заклад',
    'order_deadline'     => 'Дата виконання',
    'order_thanks'       => 'Дякуємо за ваше замовлення. Скоро із вами звяжеться наш менеджер.',
    /*
    |---------------------------------------------------------------------------
    | Order types
    |---------------------------------------------------------------------------
    */
    'type_dup'     => 'Дипломна робота',
    'type_kurs'    => 'Курсова робота',
    'type_kont'    => 'Контрольна робота',
    'type_refs'    => 'Реферат',
    'type_zvit'    => 'Звіт з практики',
    'type_magistr' => 'Магістерська робота',
    'type_article' => 'Стаття',
    'type_kpiz'    => 'КПІЗ',
    'type_prez'    => 'Презентація',
    'type_thezis'  => 'Тези',
    'type_answ'    => 'Відповіді на білети',
    'type_test'    => 'Тести',
    'type_report'  => 'Доповіді',
    'type_ese'     => 'Есе',

    'admin_type_dup'     => 'Дипломи',
    'admin_type_kurs'    => 'Курсові',
    'admin_type_kont'    => 'Контрольні',
    'admin_type_refs'    => 'Реферати',
    'admin_type_zvit'    => 'Звіти з практики',
    'admin_type_magistr' => 'Магістерські роботи',
    'admin_type_article' => 'Статті',
    'admin_type_kpiz'    => 'КПІЗ',
    'admin_type_prez'    => 'Презентації',
    'admin_type_thezis'  => 'Тези',
    'admin_type_answ'    => 'Відповіді на білети',
    'admin_type_test'    => 'Тести',
    'admin_type_report'  => 'Доповіді',
    'admin_type_ese'     => 'Есе',
    /*
    |---------------------------------------------------------------------------
    | Contacts
    |---------------------------------------------------------------------------
    */

    'contacts_heading' => 'Наші контакти',
    'contacts_skype'   => 'Skype',
    'contacts_phone'   => 'Телефон',
    'contacts_email'   => 'Пошта',

    /*
    |---------------------------------------------------------------------------
    | Article translate
    |---------------------------------------------------------------------------
    */
    'article'           => 'Огляд роботи',
    'article_author'    => 'Автор',
    'article_price'     => 'Ціна',
    'article_type'      => 'Тип',
    'article_date'      => 'Дата',
    'article_category'  => 'Категорія',
];
