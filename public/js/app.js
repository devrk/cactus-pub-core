/**
 * Created by Roman on 11.12.2016.
 */
var map;
function initMap() {
    map = new google.maps.Map(document.getElementById('location-map'), {
        center: {lat: 49.557388, lng: 25.595683},
        zoom: 15,
        scrollwheel: false,
    });
}