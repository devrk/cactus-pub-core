<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/{slug}', 'HomeController@frontPage');
Route::get('/blog/{slug}', 'HomeController@index');



Route::group(['prefix' => 'admin'], function(){
    
    Route::get('panel', 'Admin\PanelController@index');
    
    Route::get('posts', 'Admin\PostController@index');
    
    Route::get('main_page', 'Admin\SystemPageController@mainPage');
    Route::post('main_page', 'Admin\SystemPageController@saveMainPage');
    
    Route::get('system_pages', 'Admin\SystemPageController@pageList');
    Route::get('add_page', 'Admin\SystemPageController@page');
    Route::get('page/{id}', 'Admin\SystemPageController@page');
    Route::get('delete_page/{id}', 'Admin\SystemPageController@addPage');
    
    Route::post('add_page', 'Admin\SystemPageController@addPage');
    Route::post('save_page/{id}', 'Admin\SystemPageController@savePage');
    
});
