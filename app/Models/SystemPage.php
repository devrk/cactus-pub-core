<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SystemPage extends Model
{
    protected $table = 'page';
    protected $fillable = ['name','redirect','meta_keywords','meta_description','url','redirect','order','show','level','parent', 'settings', 'created_at', 'updated_at'];
    
    public function scopeGetMainPage($query)
    {
        return $query->where('url', '/')->where('level', 0)->where('parent', 0);
    }
}