<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\SystemPage as Page;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    public function index()
    {
        return view('public.home');
    }

    public function frontPage(Request $request, string $slug = '')
    {
        $pages = Page::where('url', '<>', '/')->get();
        
        return view('public.home', ['page' => $pages->all()]);
    }
}
