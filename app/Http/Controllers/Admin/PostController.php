<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.post.list');
    }
    
    public function ask(Request $request)
    {
        $data = [];
        
        for($i = 0; $i <30; $i++)
        {
            $data[] = [
                'id' => $i+1,
                'invdate' => rand(0, 10),
                'name' => str_random(8),
                'amount' => rand(0,100),
                'tax' => rand(0,100),
                'total' => rand(10,1000),
                'note' => str_random(20)
            ];
        }
        return json_encode([
            'page' => '1',
            'total' => 3,
            'records' => '30',
            'rows' => $data,
            'userdata' => [
                'amount' => 100,
                'tax' => 100,
                'total' => 100,
                'name' => 'Totals:',
            ]
        ]);
    }
}
