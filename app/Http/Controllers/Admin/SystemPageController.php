<?php

namespace App\Http\Controllers\Admin;

use DB;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Models\SystemPage as Page;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class SystemPageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function mainPage(Request $request)
    {
        $page = Page::getMainPage()->first();
        $page = empty($page) ? [] : $page;
        
        return view('admin.pages.home', compact('page'));
    }
    
    public function pageList(Request $request, int $page = 1)
    {
        $pages = Page::where('url', '<>', '/')->paginate(15);
        
        return view('admin.pages.list', ['pages' => $pages]);
    }
    
    public function page(int $id = 0)
    {
        $parent = Page::where('url', '<>', '/')->where('parent', '=', 0)->get();
        
        if($id != 0)
        {
            $page = Page::where('url', '<>', '/')->where('id', '=', $id)->first();
    
            return view('admin.pages.page', ['parents' => $parent->all(), 'page' => $page]);
        }
        return view('admin.pages.new', ['parents' => $parent->all()]);
    }
    
    public function addPage(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'             => 'required|min:3',
            'url'              => 'required',
            'meta_keywords'    => 'required|min:3',
            'meta_description' => 'required|min:3'
        ]);
    
        if( ! $validator->fails()) {
    
            $id = DB::table('page')->insertGetId(
                array_merge($request->except(['_method', '_token']),
                    [
                        'settings'   => '',
                        'updated_at' => date('Y-m-d H:i:s', time()),
                        'created_at' => date('Y-m-d H:i:s', time()),
                    ])
            );
        
            return Redirect::to('admin/page/' . $id);
        }
        else {
            return Redirect::to('admin/add_page')->withInput()->withErrors($validator);
        }
    }
    
    public function savePage(Request $request, int $id = 0)
    {
        $validator = Validator::make($request->all(), [
            'name'             => 'required|min:3',
            'url'              => 'required',
            'meta_keywords'    => 'required|min:3',
            'meta_description' => 'required|min:3'
        ]);
    
        if( ! $validator->fails()) {
        
            DB::table('page')->where('id', $id)->update(
                array_merge($request->except(['_method', '_token']),
                    [
                        'settings'   => '',
                        'updated_at' => date('Y-m-d H:i:s', time())
                    ])
            );
        
            return Redirect::to('admin/page/' . $id);
        }
        else {
            return Redirect::to('admin/page/' . $id)->withInput()->withErrors($validator);
        }
    }
    
    public function saveMainPage(Request $request, int $id = 0)
    {
        $validator = Validator::make($request->all(), [
            'name'             => 'required|min:3',
            'redirect'         => 'required|min:3',
            'meta_keywords'    => 'required|min:3',
            'meta_description' => 'required|min:3'
        ]);
        
        if( ! $validator->fails()) {
            
            Page::updateOrCreate(
            [
                'url'        => '/',
                'settings'   => '',
                'order'      => '0',
                'show'       => '1',
                'level'      => '0',
                'parent'     => '0',
            ],
            array_merge($request->except(['_method', '_token']),
                [
                    'url'        => '/',
                    'settings'   => '',
                    'order'      => '0',
                    'show'       => '1',
                    'level'      => '0',
                    'parent'     => '0',
                    'updated_at' => time(),
                    'created_at' => time(),
                ])
            );
            
            return Redirect::to('admin/main_page');
        }
        else {
            return Redirect::to('admin/main_page')->withInput()->withErrors($validator);
        }
    }
    
    public function deletePage(int $id = 0)
    {
    }
}
